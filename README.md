# Domaines Services Publicitaires

##############          Description : FR   ################
Ce fichier contient les noms de domaines de tous les services publicitaire connus que vous pouvez mettre dans votre fichier /etc/hosts pour avoir un blocages des pubs au niveau de votre OS.

#############           Description : EN   ################
This repo contains an host file which have the domain names associated with several ad services. You can copy the part you need to your /etc/hosts on your system 
to have a strong and OS level ad blocker.
Have fun!
